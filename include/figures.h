#ifndef FIGURES_H
#define FIGURES_H

/**
 * @file figures.h
 * @brief Defines structures and functions in order to create and manipulate figures in a SVG file.
 */

typedef struct Figure Figure;

typedef enum
{
    FIGURE_CIRCLE,
    FIGURE_LINE,
    FIGURE_RECTANGLE,
    FIGURE_TEXT,
    FIGURE_ELLIPSE
} FigureType;

typedef struct
{
    int x;
    int y;
} Coordinates;

typedef struct
{
    int height;
    int width;
} Dimensions;

typedef struct
{
    int stroke_width;
    char *stroke_color;
    char *fill_color;
    int visible;
    int rotation_angle;
} Style;

struct Figure
{
    FigureType type;
    char *name;
    Coordinates center;
    Figure *next;
    Style style;
    int selected;
};

typedef struct
{
    Figure base;
    int radius;
} Circle;

typedef struct
{
    Figure base;
    Coordinates end;
} Line;

typedef struct
{
    Figure base;
    Dimensions dimensions;
} Rectangle;

typedef struct
{
    Figure base;
    Dimensions dimensions;
    int radius;
} Ellipse;

typedef struct
{
    Figure base;
    char *text;
    int font_size;
    int cx;
    int cy;
} Text;

extern Figure *head;
extern Figure **selected_figures;
extern int selected_count;

/**
 * @brief Creates a circle with the given name, coordinates, and radius.
 * @param name A unique name for the circle. If a figure with the same name already exists, the function returns 1.
 * @param x The x coordinate of the center of the circle.
 * @param y The y coordinate of the center of the circle.
 * @param radius The radius of the circle.
 * @return 0 if the circle is successfully created and added to the list; 1 if a figure with the same name already exists.
 */
int create_circle(const char *name, int x, int y, int radius);
/**
 * @brief Creates a circle with the given elements
 * @param elements Circle definition and options
 * @return 0 if the circle is successfully created and added to the list; 1 when error on circle definition, -1 when error on options, 2 when name already exists.
 */
int create_circle_extended(char **elements);
/**
 * @brief Creates a line with the given name, start coordinates, and end coordinates.
 * @param name A unique name for the line. If a figure with the same name already exists, the function returns 1.
 * @param start_x The x coordinate of the start of the line.
 * @param start_y The y coordinate of the start of the line.
 * @param end_x The x coordinate of the end of the line.
 * @param end_y The y coordinate of the end of the line.
 * @return 0 if the line is successfully created and added to the list; 1 if a figure with the same name already exists.
 */
int create_line(const char *name, int start_x, int start_y, int end_x, int end_y);
/**
 * @brief Creates a line with the given elements
 * @param elements Line definition and options
 * @return 0 if the line is successfully created and added to the list; 1 when error on line definition, -1 when error on options, 2 when name already exists.
 */
int create_line_extended(char **elements);
/**
 * @brief Creates a rectangle with the given name, coordinates, and dimensions.
 * @param name A unique name for the rectangle. If a figure with the same name already exists, the function returns 1.
 * @param x The x coordinate of the top left corner of the rectangle.
 * @param y The y coordinate of the top left corner of the rectangle.
 * @param width The width of the rectangle.
 * @param height The height of the rectangle.
 * @return 0 if the rectangle is successfully created and added to the list; 1 if a figure with the same name already exists.
 */
int create_rectangle(const char *name, int x, int y, int width, int height);
/**
 * @brief Creates a rectangle with the given elements
 * @param elements Rectangle definition and options
 * @return 0 if the rectangle is successfully created and added to the list; 1 when error on rectangle definition, -1 when error on options, 2 when name already exists.
 */
int create_rectangle_extended(char **elements);
/**
 * @brief Creates a text with the given name, coordinates, and text.
 * @param name A unique name for the text. If a figure with the same name already exists, the function returns 1.
 * @param x The x coordinate of the text.
 * @param y The y coordinate of the text.
 * @param text The text to be displayed.
 * @return 0 if the text is successfully created and added to the list; 1 if a figure with the same name already exists.
 */
int create_text(const char *name, int x, int y, const char *text);
/**
 * @brief Creates a text with the given elements
 * @param elements Text definition and options
 * @return 0 if the text is successfully created and added to the list; 1 when error on text definition, -1 when error on options, 2 when name already exists.
 */
int create_text_extended(char **elements);
/**
 * @brief Creates an ellipse with the given name, coordinates, and dimensions.
 * @param name A unique name for the ellipse. If a figure with the same name already exists, the function returns 1.
 * @param x The x coordinate of the center of the ellipse.
 * @param y The y coordinate of the center of the ellipse.
 * @param width The width of the ellipse.
 * @param height The height of the ellipse.
 * @return 0 if the ellipse is successfully created and added to the list; 1 if a figure with the same name already exists.
 */
int create_ellipse(const char *name, int x, int y, int rx, int ry, int radius);
/**
 * @brief Creates an ellipse with the given elements
 * @param elements Ellipse definition and options
 * @return 0 if the ellipse is successfully created and added to the list; 1 when error on ellipse definition, -1 when error on options, 2 when name already exists.
 */
int create_ellipse_extended(char **elements);
/**
 * @brief Generates an SVG line element for the given line figure and writes it to the specified file.
 * @param file A pointer to the FILE object where the SVG line element should be written.
 * @param line A pointer to the Figure object representing the line. The object must be of type FIGURE_LINE.
 */
void generate_svg_line(FILE *file, Figure *line);
/**
 * @brief Generates an SVG rectangle element for the given rectangle figure and writes it to the specified file.
 * @param file A pointer to the FILE object where the SVG rectangle element should be written.
 * @param rectangle A pointer to the Figure object representing the rectangle. The object must be of type FIGURE_RECTANGLE.
 */
void generate_svg_rectangle(FILE *file, Figure *rectangle);
/**
 * @brief Generates an SVG text element for the given text figure and writes it to the specified file.
 * @param file A pointer to the FILE object where the SVG text element should be written.
 * @param text A pointer to the Figure object representing the text. The object must be of type FIGURE_TEXT.
 */
void generate_svg_text(FILE *file, Figure *text);
/**
 * @brief Generates an SVG circle element for the given circle figure and writes it to the specified file.
 * @param file A pointer to the FILE object where the SVG circle element should be written.
 * @param circle A pointer to the Figure object representing the circle. The object must be of type FIGURE_CIRCLE.
 */
void generate_svg_circle(FILE *file, Figure *circle);
/**
 * @brief Generates an SVG ellipse element for the given ellipse figure and writes it to the specified file.
 * @param file A pointer to the FILE object where the SVG ellipse element should be written.
 * @param ellipse A pointer to the Figure object representing the ellipse. The object must be of type FIGURE_ELLIPSE.
 */
void generate_svg_ellipse(FILE *file, Figure *ellipse);
/**
 * @brief Deletes the figure with the given name.
 * @param name The name of the figure to be deleted.
 * @return 0 if the figure is successfully deleted; 1 if a figure with the given name does not exist.
 */
int delete_figure(const char *name);
/**
 * @brief Rename a figure.
 * @param old_name The current name of the figure.
 * @param new_name The new name of the figure.
 * @return 0 if the figure is successfully renamed; 1 if a figure with the given name does not exist.
 */
int rename_figure(const char *old_name, const char *new_name);
/**
 * @brief Prints the details of all figures in the global list to stdout.
 * @return 0 if the list is successfully printed; 1 if the list is empty.
 */
int dump_figures();
/**
 * @brief Prints the details of all figures in the global list to the specified file.
 * @param file A pointer to the FILE object where the list should be printed.
 * @return 0 if the list is successfully printed; 1 if the file cannot be oppened; 2 if the list is empty.
 */
int dump_figures_to_svg(const char *filename);
/**
 * @brief Finds a figure with the given name.
 * @param name The name of the figure to be found.
 * @return A pointer to the Figure object with the given name, or NULL if a figure with the given name does not exist.
 */
Figure *find_figure_by_name(const char *name);

int apply_option(Figure *figure, const char *option);

/**
 * @brief Sets the specified options for the figure with the given name.
 * @param name The name of the figure to apply options to.
 * @param options An array of strings representing the options to apply.
 * @return 0 on success; 1 if trying to apply "FILL WITH" or "NOFILL" to a line;2 if trying to apply "FONTSIZE" to something else
 *  than text; 3 for unknown options; 4 if a figure with the given name does not exist.
 */
int set_figure_options(const char *name, char *options[]);
/**
 * @brief Sets the specified options for all figures in the global list.
 * @param options An array of strings representing the options to apply.
 * @return 0 on success; 1 if trying to apply "FILL WITH" or "NOFILL" to a line;2 if trying to apply "FONTSIZE" to something else
 *  than text; 3 for unknown options; 4 if the list of figures is empty.
 */
int set_all_figures_options(char *options[]);
/**
 * @brief Sets the specified options for all selected figures in the global list.
 * @param options An array of strings representing the options to apply.
 * @return 0 on success; 1 if trying to apply "FILL WITH" or "NOFILL" to a line;2 if trying to apply "FONTSIZE" to something else
 *  than text; 3 for unknown options; 4 if no selected figures.
 */
int set_selected_figures_options(char *options[]);
/**
 * @brief Sets the specified options for the figures with the given names.
 * @param figures An array of strings representing the names of the figures to apply options to.
 * @param options An array of strings representing the options to apply.
 * @return 0 on success; 1 if trying to apply "FILL WITH" or "NOFILL" to a line;2 if trying to apply "FONTSIZE" to something else
 *  than text; 3 for unknown options; 4 if a figure with the given name does not exist.
 */
int set_figures_options_by_name(char **figures, char *options[]);

/**
 * @brief Selects the figure with the given name.
 * @param name The name of the figure to be selected.
 * @return 0 on success; 1 if a figure with the given name does not exist; 2 if a figure with the given name is already selected.
 */
int select_figure(const char *name);
/**
 * @brief Selects all figures in the global list.
 * @return 0 on success; 1 if no created figures yet.
 */
int select_all_figures();
/**
 * @brief Deselects the figure with the given name.
 * @param name The name of the figure to be deselected.
 * @return 0 on success; 1 if a figure with the given name does not exist; 2 if a figure with the given name is already deselected.
 */
int deselect_figure(const char *name);
/**
 * @brief Deselects all figures in the global list.
 * @return 0 on success; 1 if no selected figures yet.
 */
int deselect_all_figures();

/**
 * @brief Moves the selected figures with the specified coordinates.
 * @param x The x coordinate to move the figures to.
 * @param y The y coordinate to move the figures to.
 * @return 0 on success; 1 if no selected figures yet.
 */
int move_selected_figures(int x, int y);
/**
 * @brief Moves the figures with the specified names with the specified coordinates.
 * @param figures An array of strings representing the names of the figures to move.
 * @param x The x coordinate to move the figures to.
 * @param y The y coordinate to move the figures to.
 * @return 0 on success; 1 if a figure with the given name does not exist.
 */
int move_figures_by_name(char **figures, int x, int y);
/**
 * @brief Zoom circle with the specified factor.
 * @param circle The circle to be zoomed.
 * @param factor The factor to zoom the circle with.
 */
void zoom_circle(Circle *circle, float factor);
/**
 * @brief Zoom line with the specified factor.
 * @param line The line to be zoomed.
 * @param factor The factor to zoom the line with.
 */
void zoom_line(Line *line, float factor);
/**
 * @brief Zoom rectangle with the specified factor.
 * @param rectangle The rectangle to be zoomed.
 * @param factor The factor to zoom the rectangle with.
 */
void zoom_rectangle(Rectangle *rectangle, float factor);
/**
 * @brief Zoom text with the specified factor.
 * @param text The text to be zoomed.
 * @param factor The factor to zoom the text with.
 */
void zoom_text(Text *text, float factor);
/**
 * @brief Zoom the selected figures with the specified factor.
 * @param factor The factor to zoom the figures with.
 * @return 0 on success; 1 if no selected figures yet.
 */
int zoom_selected_figures(float factor);
/**
 * @brief Rotate line with the specified angle.
 * @param line The line to be rotated.
 * @param angle The angle to rotate the line with.
 */
void rotate_line(Line *line, int angle);
/**
 * @brief Rotate rectangle with the specified angle.
 * @param rectangle The rectangle to be rotated.
 * @param angle The angle to rotate the rectangle with.
 */
void rotate_rectangle(Rectangle *rectangle, int angle);
/**
 * @brief Rotate text with the specified angle.
 * @param text The text to be rotated.
 * @param angle The angle to rotate the text with.
 */
void rotate_text(Text *text, int angle);
/**
 * @brief Rotate the selected figures with the specified angle.
 * @param angle The angle to rotate the figures with.
 * @return 0 on success; 1 if no selected figures yet.
 */
int rotate_selected_figures(int angle);
/**
 * @brief copy a circle to a new figure
 * @param base_circle the circle to be copied
 * @param new_figure_name the name of the new figure which is copy figure
 */
void copy_circle(Circle *base_circle, char *new_figure_name);
/**
 * @brief copy a line to a new figure
 * @param base_line the line to be copied
 * @param new_figure_name the name of the new figure which is copy figure
 */
void copy_line(Line *base_line, char *new_figure_name);
/**
 * @brief copy a rectangle to a new figure
 * @param base_rectangle the rectangle to be copied
 * @param new_figure_name the name of the new figure which is copy figure
 */
void copy_rectangle(Rectangle *base_rectangle, char *new_figure_name);
/**
 * @brief copy a text to a new figure
 * @param base_text the text to be copied
 * @param new_figure_name the name of the new figure which is copy figure
 */
void copy_text(Text *base_text, char *new_figure_name);
/**
 * @brief copy a ellipse to a new figure
 * @param base_ellipse the ellipse to be copied
 * @param new_figure_name the name of the new figure which is copy figure
 */
void copy_ellipse(Ellipse *base_ellipse, char *new_figure_name);
/**
 * @brief Copy the figure with the specified name.
 * @param figure the name of the figure to be copied.
 * @param new_figure_name the name of the new figure which is copy figure.
 * @return 0 on success; 1 if a figure with the given name does not exist; 2 if a figure with the new_figure_name name already exist.
 */
int copy_figure(char *figure, char *new_figure_name);

#endif
