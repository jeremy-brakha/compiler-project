BIN= create_svg
SRCDIR= src
BUILDDIR= build
BINDIR= bin
INCLUDEDIR= include

all: $(BINDIR)/$(BIN).bin

$(BINDIR)/$(BIN).bin: $(BUILDDIR)/lex.yy.o $(BUILDDIR)/$(BIN).tab.o $(BUILDDIR)/figures.o $(BUILDDIR)/copy.o $(BUILDDIR)/options.o $(BUILDDIR)/selection.o $(BUILDDIR)/transformation.o
	$(CC) -o $@ $^ -lm

$(BUILDDIR)/lex.yy.o: $(BUILDDIR)/lex.yy.c
	$(CC) -c $< -o $@ -I $(INCLUDEDIR)

$(BUILDDIR)/$(BIN).tab.o: $(BUILDDIR)/$(BIN).tab.c
	$(CC) -c $< -o $@ -I $(INCLUDEDIR)

$(BUILDDIR)/figures.o: $(SRCDIR)/figures.c
	$(CC) -c $< -o $@ -I $(INCLUDEDIR)

$(BUILDDIR)/copy.o: $(SRCDIR)/copy.c
	$(CC) -c $< -o $@ -I $(INCLUDEDIR)

$(BUILDDIR)/options.o: $(SRCDIR)/options.c
	$(CC) -c $< -o $@ -I $(INCLUDEDIR)

$(BUILDDIR)/selection.o: $(SRCDIR)/selection.c
	$(CC) -c $< -o $@ -I $(INCLUDEDIR)

$(BUILDDIR)/transformation.o: $(SRCDIR)/transformation.c
	$(CC) -c $< -o $@ -I $(INCLUDEDIR)

$(BUILDDIR)/$(BIN).tab.c $(BUILDDIR)/$(BIN).tab.h: $(SRCDIR)/$(BIN).y
	bison -d -g -v -o $(BUILDDIR)/$(BIN).tab.c $<

$(BUILDDIR)/lex.yy.c: $(SRCDIR)/$(BIN).l $(BUILDDIR)/$(BIN).tab.h
	flex -d -o $@ $<

clean_all:
	rm -fv $(BINDIR)/*.svg $(BINDIR)/$(BIN).bin $(BUILDDIR)/$(BIN).gv $(BUILDDIR)/$(BIN).output $(BUILDDIR)/lex.yy.c $(BUILDDIR)/$(BIN).tab.h $(BUILDDIR)/$(BIN).tab.c $(BUILDDIR)/$(BIN).tab.o $(BUILDDIR)/figures.o $(BUILDDIR)/copy.o  $(BUILDDIR)/options.o $(BUILDDIR)/selection.o $(BUILDDIR)/transformation.o  $(BUILDDIR)/lex.yy.o $(BUILDDIR)/$(BIN).dot

clean_all_but_svg:
	rm -fv $(BINDIR)/$(BIN).bin $(BUILDDIR)/$(BIN).gv $(BUILDDIR)/$(BIN).output $(BUILDDIR)/lex.yy.c $(BUILDDIR)/$(BIN).tab.h $(BUILDDIR)/$(BIN).tab.c $(BUILDDIR)/$(BIN).tab.o $(BUILDDIR)/figures.o $(BUILDDIR)/copy.o $(BUILDDIR)/selection.o $(BUILDDIR)/transformation.o $(BUILDDIR)/lex.yy.o $(BUILDDIR)/lex.yy.o $(BUILDDIR)/$(BIN).dot

clean_svg:
	rm -fv $(BINDIR)/*.svg
