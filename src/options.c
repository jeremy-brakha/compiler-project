#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "figures.h"

int apply_option(Figure *figure, const char *option)
{
    if (strncmp(option, "THICKNESS", 9) == 0)
    {
        int thickness;
        sscanf(option, "THICKNESS %d", &thickness);
        figure->style.stroke_width = thickness;
        return 0;
    }
    else if (strncmp(option, "FILL WITH", 9) == 0)
    {
        if (figure->type == FIGURE_LINE)
        {
            return 1;
        }
        char color[7];
        sscanf(option, "FILL WITH %s", color);
        if (figure->style.fill_color)
        {
            free(figure->style.fill_color);
        }
        figure->style.fill_color = strdup(color);
        return 0;
    }
    else if (strcmp(option, "NOFILL") == 0)
    {
        if (figure->type == FIGURE_LINE)
        {
            return 1;
        }
        if (figure->style.fill_color)
        {
            free(figure->style.fill_color);
        }
        figure->style.fill_color = NULL;
        return 0;
    }
    else if (strncmp(option, "#", 1) == 0)
    {
        if (figure->style.stroke_color)
        {
            free(figure->style.stroke_color);
        }
        figure->style.stroke_color = strdup(option);
        return 0;
    }
    else if (strcmp(option, "INVISIBLE") == 0)
    {
        figure->style.visible = 1;
        return 0;
    }
    else if (strcmp(option, "VISIBLE") == 0)
    {
        figure->style.visible = 0;
        return 0;
    }
    else if (strcmp(option, "FONTSIZE") == 0 && figure->type == FIGURE_TEXT)
    {
        int size;
        sscanf(option, "FONTSIZE %d", &size);
        Text *text = (Text *)figure;
        text->font_size = size;
        return 0;
    }
    else if (strstr(option, "FONTSIZE") != NULL && figure->type != FIGURE_TEXT)
    {
        return 2;
    }
    else
    {
        return 3;
    }
}

int set_figure_options(const char *name, char *options[])
{
    Figure *figure = find_figure_by_name(name);
    if (figure == NULL)
    {
        return 4;
    }

    for (int i = 0; options[i] != NULL; i++)
    {
        int option = apply_option(figure, options[i]);
        if (option != 0)
        {
            return option;
        }
    }
    return 0;
}

int set_all_figures_options(char *options[])
{
    Figure *current = head;
    if (current == NULL)
    {
        return 4;
    }
    while (current != NULL)
    {
        for (int i = 0; options[i] != NULL; i++)
        {
            int option = apply_option(current, options[i]);
            if (option != 0)
            {
                return option;
            }
        }
        current = current->next;
    }
    return 0;
}

int set_selected_figures_options(char *options[])
{
    if (selected_count == 0)
    {
        return 4;
    }
    for (int i = 0; i < selected_count; i++)
    {
        for (int j = 0; options[j] != NULL; j++)
        {
            apply_option(selected_figures[i], options[j]);
        }
    }
    return 0;
}

int set_figures_options_by_name(char *names[], char *options[])
{
    int option;
    for (int i = 0; names[i] != NULL; i++)
    {
        Figure *figure = find_figure_by_name(names[i]);
        if (figure == NULL)
        {
            return 4;
        }
        option = set_figure_options(names[i], options);
    }
    return option;
}
