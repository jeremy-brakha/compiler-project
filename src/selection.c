#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "figures.h"

int select_figure(const char *name)
{
    Figure *figure = find_figure_by_name(name);
    if (figure == NULL)
    {
        return 1;
    }

    if (!figure->selected)
    {
        figure->selected = 1;
        selected_count++;
        selected_figures = (Figure **)realloc(selected_figures, selected_count * sizeof(Figure *));
        selected_figures[selected_count - 1] = figure;
        printf("Figure %s selected.\n", name);
    }
    else
    {
        return 2;
    }
    return 0;
}

int select_all_figures()
{
    Figure *current = head;
    if (current == NULL)
    {
        return 1;
    }
    while (current != NULL)
    {
        if (!current->selected)
        {
            current->selected = 1;
            selected_count++;
            selected_figures = (Figure **)realloc(selected_figures, selected_count * sizeof(Figure *));
            selected_figures[selected_count - 1] = current;
            printf("Figure %s selected.\n", current->name);
        }
        current = current->next;
    }
    return 0;
}

int deselect_figure(const char *name)
{
    Figure *figure = find_figure_by_name(name);
    if (figure == NULL)
    {
        return 1;
    }

    if (figure->selected)
    {
        figure->selected = 0;
        for (int i = 0; i < selected_count; i++)
        {
            if (selected_figures[i] == figure)
            {
                selected_figures[i] = selected_figures[selected_count - 1];
                selected_count--;
                selected_figures = (Figure **)realloc(selected_figures, selected_count * sizeof(Figure *));
                break;
            }
        }
        printf("Figure %s deselected.\n", name);
    }
    else
    {
        return 2;
    }
    return 0;
}

int deselect_all_figures()
{
    if (selected_count == 0)
    {
        return 1;
    }
    for (int i = 0; i < selected_count; i++)
    {
        selected_figures[i]->selected = 0;
        printf("Figure %s deselected.\n", selected_figures[i]->name);
    }
    free(selected_figures);
    selected_figures = NULL;
    selected_count = 0;
    return 0;
}
