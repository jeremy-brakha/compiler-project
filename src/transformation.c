#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "figures.h"

int move_selected_figures(int x, int y)
{
    if (selected_count == 0)
    {
        return 1;
    }

    for (int i = 0; i < selected_count; i++)
    {
        if (selected_figures[i]->type == FIGURE_LINE)
        {
            Figure *figure = selected_figures[i];
            Line *line = (Line *)figure;
            figure->center.x += x;
            figure->center.y += y;
            line->end.x += x;
            line->end.y += y;
        }
        Figure *figure = selected_figures[i];
        figure->center.x += x;
        figure->center.y += y;
        printf("Figure %s moved.\n", figure->name);
    }
    return 0;
}

int move_figures_by_name(char **figures, int x, int y)
{
    for (int i = 0; figures[i] != NULL; i++)
    {
        Figure *figure = find_figure_by_name(figures[i]);
        if (figure == NULL)
        {
            return 1;
        }
        else if (figure->type == FIGURE_LINE)
        {
            Line *line = (Line *)figure;
            figure->center.x += x;
            figure->center.y += y;
            line->end.x += x;
            line->end.y += y;
            printf("Figure %s moved.\n", figure->name);
        }
        else
        {
            figure->center.x += x;
            figure->center.y += y;
            printf("Figure %s moved.\n", figure->name);
        }
    }
    free(figures);
    return 0;
}

void zoom_circle(Circle *circle, float factor)
{
    circle->radius *= factor;
    printf("Figure %s zoomed.\n", circle->base.name);
}

void zoom_line(Line *line, float factor)
{
    float x_center = (line->base.center.x + line->end.x) / 2;
    float y_center = (line->base.center.y + line->end.y) / 2;
    line->base.center.x = x_center + (line->base.center.x - x_center) * factor;
    line->base.center.y = y_center + (line->base.center.y - y_center) * factor;

    line->end.x = x_center + (line->end.x - x_center) * factor;
    line->end.y = y_center + (line->end.y - y_center) * factor;
    printf("Figure %s zoomed.\n", line->base.name);
}

void zoom_rectangle(Rectangle *rectangle, float factor)
{
    float x_center = rectangle->base.center.x + rectangle->dimensions.width / 2;
    float y_center = rectangle->base.center.y + rectangle->dimensions.height / 2;
    rectangle->dimensions.width *= factor;
    rectangle->dimensions.height *= factor;
    rectangle->base.center.x = x_center - rectangle->dimensions.width / 2;
    rectangle->base.center.y = y_center - rectangle->dimensions.height / 2;
    printf("Figure %s zoomed.\n", rectangle->base.name);
}

void zoom_text(Text *text, float factor)
{
    // Text *text = (Text *)figure;
    float new_font_size = text->font_size * factor;
    float width = strlen(text->text) * text->font_size * 0.6;
    float height = text->font_size;
    float x_center = text->base.center.x + width / 2;
    float y_center = text->base.center.y - height / 2;
    text->font_size = new_font_size;
    float new_width = strlen(text->text) * new_font_size * 0.6;
    float new_height = new_font_size;
    text->base.center.x = x_center - new_width / 2;
    text->base.center.y = y_center + new_height / 2;
    printf("Figure %s zoomed.\n", text->base.name);
}

int zoom_selected_figures(float factor)
{
    if (selected_count == 0)
    {
        return 1;
    }

    for (int i = 0; i < selected_count; i++)
    {
        Figure *figure = selected_figures[i];
        float x_center, y_center;
        switch (figure->type)
        {
        case FIGURE_CIRCLE:
            zoom_circle((Circle *)figure, factor);
            break;
        case FIGURE_LINE:
            zoom_line((Line *)figure, factor);
            break;
        case FIGURE_RECTANGLE:
            zoom_rectangle((Rectangle *)figure, factor);
            break;
        case FIGURE_TEXT:
            zoom_text((Text *)figure, factor);
            break;
        case FIGURE_ELLIPSE:
            // TODO
            printf("Not implemented yet.\n");
            break;
        }
    }
    return 0;
}

void rotate_line(Line *line, int angle)
{
    float x_center = (line->base.center.x + line->end.x) / 2;
    float y_center = (line->base.center.y + line->end.y) / 2;
    float dx1 = line->base.center.x - x_center;
    float dy1 = line->base.center.y - y_center;
    float dx2 = line->end.x - x_center;
    float dy2 = line->end.y - y_center;
    float angle_rad = angle * M_PI / 180;
    line->base.center.x = x_center + dx1 * cos(angle_rad) - dy1 * sin(angle_rad);
    line->base.center.y = y_center + dx1 * sin(angle_rad) + dy1 * cos(angle_rad);
    line->end.x = x_center + dx2 * cos(angle_rad) - dy2 * sin(angle_rad);
    line->end.y = y_center + dx2 * sin(angle_rad) + dy2 * cos(angle_rad);
    printf("Figure %s rotated.\n", line->base.name);
}

void rotate_rectangle(Rectangle *rectangle, int angle)
{
    rectangle->base.style.rotation_angle = angle;
    printf("Figure %s rotated.\n", rectangle->base.name);
}

void rotate_text(Text *text, int angle)
{
    int width = 0;
    int height = text->font_size;
    int text_length = strlen(text->text);
    float width_factor = height * 0.6;
    width = text_length * width_factor;

    text->base.style.rotation_angle = angle;
    text->cx = text->base.center.x + width / 2;
    text->cy = text->base.center.y + height / 2;
    printf("Figure %s rotated.\n", text->base.name);
}

int rotate_selected_figures(int angle)
{
    if (selected_count == 0)
    {
        return 1;
    }

    for (int i = 0; i < selected_count; i++)
    {
        Figure *figure = selected_figures[i];
        float center_x, center_y, dx1, dy1, dx2, dy2, width_factor;
        int cx, cy, width, height, text_length;
        switch (figure->type)
        {
        case FIGURE_CIRCLE:
            fprintf(stderr, "Error: Not necessary to rotate circle.\n");
            break;
        case FIGURE_LINE:
            rotate_line((Line *)figure, angle);
            break;
        case FIGURE_RECTANGLE:
            rotate_rectangle((Rectangle *)figure, angle);
            break;
        case FIGURE_TEXT:
            rotate_text((Text *)figure, angle);
            break;
        case FIGURE_ELLIPSE:
            // TODO
            printf("Not implemented yet.\n");
            break;
        }
    }
    return 0;
}
