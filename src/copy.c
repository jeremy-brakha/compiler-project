#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "figures.h"

int copy_figure(char *figure, char *new_figure_name)
{
    Figure *base_figure = find_figure_by_name(figure);
    if (base_figure == NULL)
    {
        return 1;
    }
    Figure *destination_figure = find_figure_by_name(new_figure_name);
    if (destination_figure != NULL)
    {
        return 2;
    }

    switch (base_figure->type)
    {
    case FIGURE_CIRCLE:
        copy_circle((Circle *)base_figure, new_figure_name);
        break;
    case FIGURE_LINE:
        copy_line((Line *)base_figure, new_figure_name);
        break;
    case FIGURE_RECTANGLE:
        copy_rectangle((Rectangle *)base_figure, new_figure_name);
        break;
    case FIGURE_TEXT:
        copy_text((Text *)base_figure, new_figure_name);
        break;
    case FIGURE_ELLIPSE:
        copy_ellipse((Ellipse *)base_figure, new_figure_name);
        break;
    }
    return 0;
}

void copy_circle(Circle *base_circle, char *new_figure_name)
{
    Circle *new_circle = (Circle *)malloc(sizeof(Circle));
    new_circle->base.type = FIGURE_CIRCLE;
    new_circle->base.name = new_figure_name;
    new_circle->base.center = base_circle->base.center;
    new_circle->base.style = base_circle->base.style;
    new_circle->radius = base_circle->radius;

    new_circle->base.next = head;
    head = (Figure *)new_circle;
    printf("New circle %s copied.\n", new_figure_name);
}

void copy_line(Line *base_line, char *new_figure_name)
{
    Line *new_line = (Line *)malloc(sizeof(Line));
    new_line->base.type = FIGURE_LINE;
    new_line->base.name = new_figure_name;
    new_line->base.center = base_line->base.center;

    new_line->base.style = base_line->base.style;
    new_line->end = base_line->end;

    new_line->base.next = head;
    head = (Figure *)new_line;
    printf("New line %s copied.\n", new_figure_name);
}

void copy_rectangle(Rectangle *base_rectangle, char *new_figure_name)
{
    Rectangle *new_rectangle = (Rectangle *)malloc(sizeof(Rectangle));
    new_rectangle->base.type = FIGURE_RECTANGLE;
    new_rectangle->base.name = new_figure_name;
    new_rectangle->base.center = base_rectangle->base.center;
    new_rectangle->base.style = base_rectangle->base.style;
    new_rectangle->base.style.rotation_angle = base_rectangle->base.style.rotation_angle;
    new_rectangle->dimensions.width = base_rectangle->dimensions.width;
    new_rectangle->dimensions.height = base_rectangle->dimensions.height;

    new_rectangle->base.next = head;
    head = (Figure *)new_rectangle;
    printf("New rectangle %s copied.\n", new_figure_name);
}

void copy_text(Text *base_text, char *new_figure_name)
{
    Text *new_text = (Text *)malloc(sizeof(Text));
    new_text->base.type = FIGURE_TEXT;
    new_text->base.name = new_figure_name;
    new_text->base.center = base_text->base.center;
    new_text->base.style = base_text->base.style;
    new_text->base.style.rotation_angle = base_text->base.style.rotation_angle;
    new_text->font_size = base_text->font_size;
    new_text->cx = base_text->cx;
    new_text->cy = base_text->cy;
    new_text->text = base_text->text;

    new_text->base.next = head;
    head = (Figure *)new_text;
    printf("New text %s copied.\n", new_figure_name);
}

void copy_ellipse(Ellipse *base_ellipse, char *new_figure_name)
{
    Ellipse *new_ellipse = (Ellipse *)malloc(sizeof(Ellipse));
    new_ellipse->base.type = FIGURE_ELLIPSE;
    new_ellipse->base.name = new_figure_name;
    new_ellipse->base.center = base_ellipse->base.center;
    new_ellipse->base.style = base_ellipse->base.style;
    new_ellipse->dimensions.width = base_ellipse->dimensions.width;
    new_ellipse->dimensions.height = base_ellipse->dimensions.height;

    new_ellipse->base.next = head;
    head = (Figure *)new_ellipse;
    printf("New ellipse %s copied.\n", new_figure_name);
}
