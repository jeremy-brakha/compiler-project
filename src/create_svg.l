%{
#include "create_svg.tab.h"
%}

name [a-zA-Z][-_a-zA-Z0-9]{0,19}
color [#][0-9a-fA-F]{3,6}
num [0-9]+
real {num}\.{num}
dimension {num}x{num}
string \"[^\"\\]*\"

%option yylineno
%option noyywrap
%%

(?i:CREATE)     { return CREATE; }
(?i:CIRCLE)     { return CIRCLE; }
(?i:AT)         { return AT; }
(?i:RADIUS)     { return RADIUS; }
(?i:DUMP)       { return DUMP; }
(?i:DELETE)     { return DELETE; }
(?i:RENAME)     { return RENAME; }
(?i:WITH)       { return WITH; }
(?i:SET)        { return SET; }
(?i:THICKNESS)  { return THICKNESS; }
(?i:FILL)       { return FILL; }
(?i:NOFILL)     { return NOFILL; }
(?i:INVISIBLE)  { return INVISIBLE; }
(?i:VISIBLE)    { return VISIBLE; }
(?i:LINE)       {  return LINE; }
(?i:RECTANGLE)  {  return RECTANGLE; }
(?i:TEXT)       {  return TEXT; }
(?i:SELECT)     {  return SELECT; }
(?i:DESELECT)   {  return DESELECT; }
(?i:ALL)        {  return ALL; }
(?i:MOVE)       {  return MOVE; }
(?i:ZOOM)       {  return ZOOM; }
(?i:ROTATE)     {  return ROTATE; }
(?i:COPY)       {  return COPY; }
(?i:FONTSIZE)   {  return FONTSIZE; }
(?i:ELLIPSE)    { return ELLIPSE; }
","             { return COMMA; }
"("             { return LPAREN; }
")"             { return RPAREN; }
[ \t]+   ;

{string} {
    yylval.string = strdup(yytext);
    return STRING;
}

{name} {
    yylval.name = strdup(yytext);
    return NAME;
}

{color} {
    yylval.color = strdup(yytext);
    return COLOR;
}

{dimension} {
    yylval.dimension = strdup(yytext);
    return DIM;
}

{num} {
    yylval.num = atoi(yytext);
    return NUM;
}

{real} {
    yylval.real = atof(yytext);
    return REAL;
}

[\n;]       { return EOL; }

. {}

%%
