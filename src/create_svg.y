%{
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int yylex();
extern int yylineno;
extern FILE *yyin;
extern char* yytext;
void yyerror(const char* s);
int interactive_mode;
%}

%code requires {
#include "figures.h"
}

%define parse.error verbose

%union {
    int num;
    float real;
    char* name;
    char* color;
    char* coord;
    char* option;
    char** options_list;
    char* dimension;
    char* string;
    char** name_list;
    Coordinates coords;
}

%token <name> NAME
%token <color> COLOR
%token <num> NUM
%token <real> REAL
%token CREATE CIRCLE AT RADIUS EOL DUMP DELETE RENAME WITH SET THICKNESS FILL NOFILL INVISIBLE VISIBLE LINE RECTANGLE TEXT SELECT DESELECT ALL MOVE ZOOM ROTATE COPY FONTSIZE ELLIPSE
%token <dimension> DIM
%token <string> STRING
%token COMMA LPAREN RPAREN

%type <option> option
%type <options_list> options_list
%type <name_list> name_list
%type <coords> coord
%type <options_list> create_elements
%type <options_list> create_element

%%

commands: command EOL commands
        |
        ;

command: create_circle_command
        | create_rectangle_command
        | create_line_command
        | create_text_command
        | create_ellipse_command
        | dump_command
        | delete_command
        | rename_command
        | set_command
        | select_command
        | deselect_command
        | move_command
        | zoom_command
        | rotate_command
        | copy_command
       ;

coord: LPAREN NUM COMMA NUM RPAREN { $$ = (Coordinates) {$2, $4}; }
                 ;

create_circle_command: CREATE CIRCLE create_elements { int extended_circle = create_circle_extended($3); if (extended_circle == 1) {
                        yyerror("Error on circle definition");
                      } else if (extended_circle == -1) {
                        yyerror("Error on options");
                      } else if (extended_circle == 2) {
                        yyerror("Figure with such name already exists");
                      } else if (extended_circle == 3) {
                        yyerror("Given definition not valid for this figure");
                      }
                    }
                   ;
      
create_elements: create_element
               | create_elements create_element {
                   int count1 = 0, count2 = 0;
                   for (; $1[count1] != NULL; count1++);
                   for (; $2[count2] != NULL; count2++);
                   $$ = realloc($1, (count1 + count2 + 1) * sizeof(char*));
                   memcpy($$ + count1, $2, count2 * sizeof(char*));
                   $$[count1 + count2] = NULL;
                   free($2);
                 }
               ;

create_element: NAME { $$ = malloc(2 * sizeof(char*)); asprintf(&$$[0], "NAME %s", $1); $$[1] = NULL; }
              | RADIUS NUM { $$ = malloc(2 * sizeof(char*)); asprintf(&$$[0], "RADIUS %d", $2); $$[1] = NULL; }
              | AT coord { $$ = malloc(2 * sizeof(char*)); asprintf(&$$[0], "AT COORD (%d,%d)", $2.x, $2.y); $$[1] = NULL; }
              | coord coord { $$ = malloc(3 * sizeof(char*)); asprintf(&$$[0], "START COORD (%d,%d)", $1.x, $1.y); asprintf(&$$[1], "END COORD (%d,%d)", $2.x, $2.y); $$[2] = NULL; }
              | DIM { $$ = malloc(2 * sizeof(char*)); asprintf(&$$[0], "DIM %s", $1); $$[1] = NULL; }
              | STRING { $$ = malloc(2 * sizeof(char*)); asprintf(&$$[0], "STRING %s", $1); $$[1] = NULL; }
              | option { $$ = malloc(2 * sizeof(char*)); $$[0] = $1; $$[1] = NULL; }
              | RADIUS DIM { $$ = malloc(2 * sizeof(char*)); asprintf(&$$[0], "RADIUS DIM %s", $2); $$[1] = NULL; }
              ;



dump_command: DUMP { if (dump_figures()) {
                        yyerror("No figures created yet");
              }}
              | DUMP STRING { int dump = dump_figures_to_svg($2); if (dump == 1) {
                        yyerror("Can't write in file");
              } else if (dump == 2) {
                        yyerror("No figures created yet");
              }}
              ;

delete_command: DELETE NAME { if (delete_figure($2)) {
                        yyerror("Figure with name doesn't exist");
              }}
              ;

rename_command: RENAME NAME WITH NAME { int rename = rename_figure($2, $4); if (rename == 1) {
                        yyerror("No figure with such name");
              } else if (rename == 2) {
                        yyerror("Figure with new name already exists");
              }}

set_command: SET name_list options_list { int set = set_figures_options_by_name($2, $3); if (set == 1) {
                        yyerror("Cannot fill a line");
              } else if (set == 2) {
                        yyerror("FONTSIZE option is only valid for text figures");
              } else if (set == 3)
              {
                yyerror("Unrecognized option");
              } else if (set == 4) {
                yyerror("No figure with such name");
              } }
            | SET ALL options_list { int set = set_all_figures_options($3); if (set == 1) {
                        yyerror("Cannot fill a line");
              } else if (set == 2) {
                        yyerror("FONTSIZE option is only valid for text figures");
              } else if (set == 3)
              {
                yyerror("Unrecognized option");
              } else if (set == 4) {
                yyerror("No figures created yet");
              } }
            | SET options_list { int set = set_selected_figures_options($2); if (set == 1) {
                        yyerror("Cannot fill a line");
              } else if (set == 2) {
                        yyerror("FONTSIZE option is only valid for text figures");
              } else if (set == 3)
              {
                yyerror("Unrecognized option");
              } else if (set == 4) {
                yyerror("No figures selected");
              } }
           ;

options_list: option { $$ = malloc(2 * sizeof(char*)); $$[0] = $1; $$[1] = NULL; }
            | options_list option {
                int count = 0;
                for (; $1[count] != NULL; count++);
                $$ = realloc($1, (count + 2) * sizeof(char*));
                $$[count] = $2;
                $$[count + 1] = NULL;
              }
            ;

option: COLOR { $$ = $1; }
          | THICKNESS NUM { asprintf(&$$, "THICKNESS %d", $2); }
          | FILL WITH COLOR { asprintf(&$$, "FILL WITH %s", $3); }
          | NOFILL { $$ = "NOFILL"; }
          | INVISIBLE { $$ = "INVISIBLE"; }
          | VISIBLE { $$ = "VISIBLE"; }
          | FONTSIZE NUM { asprintf(&$$, "FONTSIZE %d", $2); }
          ;

create_line_command: CREATE LINE create_elements { int extended_line = create_line_extended($3); if (extended_line == 1) {
                        yyerror("Error on line definition");
                      } else if (extended_line == -1) {
                        yyerror("Error on options");
                      } else if (extended_line == 2) {
                        yyerror("Figure with such name already exists");
                      } else if (extended_line == 3) {
                        yyerror("Given definition not valid for this figure");
                      }
                    }
                   ;

create_rectangle_command: CREATE RECTANGLE create_elements { int extended_rectangle = create_rectangle_extended($3); if (extended_rectangle == 1) {
                        yyerror("Error on rectangle definition");
                      } else if (extended_rectangle == -1) {
                        yyerror("Error on options");
                      } else if (extended_rectangle == 2) {
                        yyerror("Figure with such name already exists");
                      } else if (extended_rectangle == 3) {
                        yyerror("Given definition not valid for this figure");
                      }
                    }
                   ;

create_text_command: CREATE TEXT create_elements { int extended_text = create_text_extended($3); if (extended_text == 1) {
                        yyerror("Error on rectangle definition");
                      } else if (extended_text == -1) {
                        yyerror("Error on options");
                      } else if (extended_text == 2) {
                        yyerror("Figure with such name already exists");
                      } else if (extended_text == 3) {
                        yyerror("Given definition not valid for this figure");
                      }
                    }
                   ;

create_ellipse_command: CREATE ELLIPSE create_elements { int extended_ellipse = create_ellipse_extended($3); if (extended_ellipse == 1) {
                        yyerror("Error on ellipse definition");
                      } else if (extended_ellipse == -1) {
                        yyerror("Error on options");
                      } else if (extended_ellipse == 2) {
                        yyerror("Figure with such name already exists");
                      } else if (extended_ellipse == 3) {
                        yyerror("Given definition not valid for this figure");
                      }
                    }
                   ;
                     
name_list: NAME { $$ = malloc(2 * sizeof(char*)); $$[0] = $1; $$[1] = NULL; }
         | NAME COMMA name_list { 
             int count = 0;
             for (; $3[count] != NULL; count++);
             $$ = malloc((count + 2) * sizeof(char*));
             $$[0] = $1;
             memcpy($$ + 1, $3, (count + 1) * sizeof(char*));
             free($3);
           }
         ;

select_command: SELECT name_list { 
                  for (int i = 0; $2[i] != NULL; i++) {
                      int select = select_figure($2[i]);
                      if (select == 1) {
                        yyerror("No figure with such name");
                      } else if (select == 2) {
                        yyerror("Figure is already selected");
                      }
                  }
                  free($2);
                }
              | SELECT ALL { int select = select_all_figures(); if (select == 1) {
                        yyerror("No figures created yet");
              }}
              ;

deselect_command: DESELECT name_list { 
                    for (int i = 0; $2[i] != NULL; i++) {
                        int deselect = deselect_figure($2[i]);
                        if (deselect == 1) {
                          yyerror("No figure with such name");
                        } else if (deselect == 2) {
                          yyerror("Figure is not selected");
                        }
                    }
                    free($2);
                  }
                | DESELECT ALL { int deselect = deselect_all_figures(); if (deselect == 1) {
                        yyerror("No figures selected"); }}
                ;

move_command: MOVE coord { int move = move_selected_figures($2.x, $2.y); if (move == 1) {
                        yyerror("No figures selected");
              }}
            | MOVE name_list coord { int move = move_figures_by_name($2, $3.x, $3.y); if (move == 1) {
                        yyerror("No figure with such name");
              }}
            ;

zoom_command: ZOOM REAL { int zoom = zoom_selected_figures($2); if (zoom == 1) {
                        yyerror("No figures selected");
              }}
            ;

rotate_command: ROTATE NUM { int rotate = rotate_selected_figures($2); if (rotate == 1) {
                        yyerror("No figures selected");
              }}
              ;

copy_command: COPY NAME NAME { int copy = copy_figure($2, $3); if (copy == 1) {
                        yyerror("No figure with such name");
              } else if (copy == 2) {
                        yyerror("Figure with the same name already exists");
              }}
            ;

%%

void yyerror(const char* s) {
  fprintf(stderr, "Error: %s at line %d\n", s, yylineno);
  if (interactive_mode == 0) {
    exit(1);
  }
}

int main(int argc, char** argv) {
  if (argc > 1) {
    fprintf(stderr, "Non-interactive mode\n");
    interactive_mode = 0;
    FILE* file = fopen(argv[1], "r");
    if (file == NULL) {
      fprintf(stderr, "Error : cannot open file %s\n", argv[1]);
      return 1;
    }
    yyin = file;

    if (yyparse()) {
      fprintf(stderr, "Error : cannot parse file %s\n", argv[1]);
      fclose(file);
      return 1;
    }
    fclose(file);
  } else {
    fprintf(stderr, "Interactive mode\n");
    interactive_mode = 1;
    yyparse();
    return 0;
  }
}

