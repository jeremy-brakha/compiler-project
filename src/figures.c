#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "figures.h"

Figure *head = NULL;
Figure **selected_figures = NULL;
int selected_count = 0;

int create_circle_extended(char **elements)
{
    int x = -1, y = -1, radius = -1, width = -1, height = -1, dx = -1, dy = -1;
    char name[21], text[101];
    Figure *figure;

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "RADIUS %d", &radius) == 1)
        {
            continue;
        }
        else if (sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "NAME %s", name) == 1)
        {
            continue;
        }
        else if (sscanf(elements[i], "RADIUS DIM %dx%d", &width, &height) == 2 || sscanf(elements[i], "DIM %dx%d", &width, &height) == 2 || sscanf(elements[i], "STRING %s", text) == 1 || sscanf(elements[i], "START COORD (%d,%d)", &dx, &dy) == 2 || sscanf(elements[i], "END COORD (%d,%d)", &dx, &dy) == 2)
        {
            return 3;
        }
    }

    if (x == -1 || y == -1 || radius == -1)
    {
        return 1;
    }

    if (create_circle(name, x, y, radius) != 0)
    {
        return 2;
    }

    figure = find_figure_by_name(name);

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "NAME %s", name) == 1 || sscanf(elements[i], "RADIUS %d", &radius) == 1 || sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2)
        {
            continue;
        }
        if (apply_option(figure, elements[i]) != 0)
        {
            return -1;
        }
    }

    return 0;
}

int create_circle(const char *name, int x, int y, int radius)
{
    if (find_figure_by_name(name))
    {
        return 1;
    }
    printf("Creating circle %s at (%d,%d) with radius %d\n", name, x, y, radius);
    Circle *circle = (Circle *)malloc(sizeof(Circle));
    circle->base.type = FIGURE_CIRCLE;
    circle->base.name = strdup(name);

    circle->base.center.x = x;
    circle->base.center.y = y;
    circle->base.style.stroke_width = 1;

    circle->radius = radius;
    circle->base.next = head;
    head = (Figure *)circle;
    return 0;
}

int create_line_extended(char **elements)
{
    int x = -1, y = -1, width = -1, height = -1, dx = -1, dy = -1;
    char name[21], text[101];
    Figure *figure;

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "START COORD (%d,%d)", &x, &y) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "END COORD (%d,%d)", &dx, &dy) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "NAME %s", name) == 1)
        {
            continue;
        }
        else if (sscanf(elements[i], "RADIUS DIM %dx%d", &width, &height) == 2 || sscanf(elements[i], "DIM %dx%d", &width, &height) == 2 || sscanf(elements[i], "STRING %s", text) == 1 || sscanf(elements[i], "RADIUS %d", &width) == 1 || sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2)
        {
            return 3;
        }
    }

    if (x == -1 || y == -1 || dx == -1 || dy == -1)
    {
        return 1;
    }

    if (create_line(name, x, y, dx, dy) != 0)
    {
        return 2;
    }

    figure = find_figure_by_name(name);

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "NAME %s", name) == 1 || sscanf(elements[i], "START COORD (%d,%d)", &x, &y) == 2 || sscanf(elements[i], "END COORD (%d,%d)", &dx, &dy) == 2)
        {
            continue;
        }
        if (apply_option(figure, elements[i]) != 0)
        {
            return -1;
        }
    }

    return 0;
}

int create_line(const char *name, int start_x, int start_y, int end_x, int end_y)
{
    if (find_figure_by_name(name))
    {
        return 1;
    }
    printf("Creating line %s from (%d,%d) to (%d,%d)\n", name, start_x, start_y, end_x, end_y);
    Line *line = (Line *)malloc(sizeof(Line));
    line->base.type = FIGURE_LINE;
    line->base.name = strdup(name);

    line->base.center.x = start_x;
    line->base.center.y = start_y;
    line->end.x = end_x;
    line->end.y = end_y;
    line->base.style.stroke_width = 1;
    line->base.style.stroke_color = strdup("black");

    line->base.next = head;
    head = (Figure *)line;
    return 0;
}

int create_rectangle_extended(char **elements)
{
    int x = -1, y = -1, width = -1, height = -1, dx = -1, dy = -1, r = -1;
    char name[21], text[101];
    Figure *figure;

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "DIM %dx%d", &width, &height) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "NAME %s", name) == 1)
        {
            continue;
        }
        else if (sscanf(elements[i], "RADIUS DIM %dx%d", &dx, &dy) == 2 || sscanf(elements[i], "STRING %s", text) == 1 || sscanf(elements[i], "RADIUS %d", &r) == 1 || sscanf(elements[i], "START COORD (%d,%d)", &dx, &dy) == 2 || sscanf(elements[i], "END COORD (%d,%d)", &dx, &dy) == 2)
        {
            return 3;
        }
    }

    if (x == -1 || y == -1 || width == -1 || height == -1)
    {
        return 1;
    }

    if (create_rectangle(name, x, y, width, height) != 0)
    {
        return 2;
    }

    figure = find_figure_by_name(name);

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "NAME %s", name) == 1 || sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2 || sscanf(elements[i], "DIM %dx%d", &width, &height) == 2)
        {
            continue;
        }
        if (apply_option(figure, elements[i]) != 0)
        {
            return -1;
        }
    }

    return 0;
}

int create_rectangle(const char *name, int x, int y, int width, int height)
{
    if (find_figure_by_name(name))
    {
        return 1;
    }
    printf("Creating rectangle %s at (%d,%d) with dimensions %dx%d\n", name, x, y, width, height);
    Rectangle *rectangle = (Rectangle *)malloc(sizeof(Rectangle));
    rectangle->base.type = FIGURE_RECTANGLE;
    rectangle->base.name = strdup(name);

    rectangle->base.center.x = x;
    rectangle->base.center.y = y;
    rectangle->dimensions.width = width;
    rectangle->dimensions.height = height;
    rectangle->base.style.stroke_width = 1;

    rectangle->base.next = head;
    head = (Figure *)rectangle;
    return 0;
}

int create_text_extended(char **elements)
{
    int x = -1, y = -1, dx = -1, dy = -1, radius = -1, font_size = -1, width = -1, height = -1;
    char name[21], text[101];
    Figure *figure;

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "NAME %s", name) == 1)
        {
            continue;
        }
        else if (sscanf(elements[i], "STRING %s", text) == 1)
        {
            continue;
        }
        else if (sscanf(elements[i], "RADIUS DIM %dx%d", &width, &height) == 2 || sscanf(elements[i], "DIM %dx%d", &width, &height) == 2 || sscanf(elements[i], "RADIUS %d", &radius) == 1 || sscanf(elements[i], "START COORD (%d,%d)", &dx, &dy) == 2 || sscanf(elements[i], "END COORD (%d,%d)", &dx, &dy) == 2)
        {
            return 3;
        }
    }

    if (x == -1 || y == -1)
    {
        return 1;
    }

    if (create_text(name, x, y, text) != 0)
    {
        return 2;
    }

    figure = find_figure_by_name(name);

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "NAME %s", name) == 1 || sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2 || sscanf(elements[i], "STRING %s", text) == 1)
        {
            continue;
        }
        if (apply_option(figure, elements[i]) != 0)
        {
            return -1;
        }
    }

    return 0;
}

int create_text(const char *name, int x, int y, const char *text)
{
    if (find_figure_by_name(name))
    {
        return 1;
    }
    printf("Creating text \"%s\" at (%d,%d) with text %s\n", name, x, y, text);
    Text *text_figure = (Text *)malloc(sizeof(Text));
    text_figure->base.type = FIGURE_TEXT;
    text_figure->base.name = strdup(name);

    text_figure->base.center.x = x;
    text_figure->base.center.y = y;
    text_figure->text = strdup(text);
    text_figure->base.style.stroke_width = 1;
    text_figure->font_size = 16;

    text_figure->base.next = head;
    head = (Figure *)text_figure;
    return 0;
}

int create_ellipse_extended(char **elements)
{
    int x = -1, y = -1, dx = -1, dy = -1, radius = -1, rx = -1, ry = -1;
    char name[21], text[101];
    Figure *figure;

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "NAME %s", name) == 1)
        {
            continue;
        }
        else if (sscanf(elements[i], "RADIUS DIM %dx%d", &rx, &ry) == 2)
        {
            continue;
        }
        else if (sscanf(elements[i], "RADIUS %d", &radius) == 1 || sscanf(elements[i], "DIM %dx%d", &dx, &dy) == 2 || sscanf(elements[i], "START COORD (%d,%d)", &dx, &dy) == 2 || sscanf(elements[i], "END COORD (%d,%d)", &dx, &dy) == 2 || sscanf(elements[i], "STRING %s", text) == 1)
        {
            return 3;
        }
    }

    if (x == -1 || y == -1 || rx == -1 || ry == -1)
    {
        return 1;
    }

    if (create_ellipse(name, x, y, rx, ry, radius) != 0)
    {
        return 2;
    }

    figure = find_figure_by_name(name);

    for (int i = 0; elements[i] != NULL; i++)
    {
        if (sscanf(elements[i], "NAME %s", name) == 1 || sscanf(elements[i], "AT COORD (%d,%d)", &x, &y) == 2 || sscanf(elements[i], "RADIUS DIM %dx%d", &rx, &ry) == 2)
        {
            continue;
        }
        printf("Applying option \"%s\" to figure \"%s\"\n", elements[i], name);
        if (apply_option(figure, elements[i]) != 0)
        {
            return -1;
        }
    }

    return 0;
}

int create_ellipse(const char *name, int x, int y, int rx, int ry, int radius)
{
    if (find_figure_by_name(name))
    {
        return 1;
    }
    printf("Creating ellipse \"%s\" at (%d,%d) with dimensions %dx%d\n", name, x, y, rx, ry);
    Ellipse *ellipse = (Ellipse *)malloc(sizeof(Ellipse));
    ellipse->base.type = FIGURE_ELLIPSE;
    ellipse->base.name = strdup(name);

    ellipse->base.center.x = x;
    ellipse->base.center.y = y;
    ellipse->dimensions.width = rx;
    ellipse->dimensions.height = ry;
    ellipse->base.style.stroke_width = 1;

    ellipse->base.next = head;
    head = (Figure *)ellipse;
    return 0;
}

int dump_figures()
{
    Figure *current = head;
    if (current == NULL)
    {
        return 1;
    }
    while (current != NULL)
    {
        if (current->type == FIGURE_CIRCLE)
        {
            Circle *circle = (Circle *)current;
            printf("Circle %s at (%d,%d) with radius %d\n", current->name, circle->base.center.x, circle->base.center.y, circle->radius);
        }
        else if (current->type == FIGURE_LINE)
        {
            Line *line = (Line *)current;
            printf("Line %s from (%d,%d) to (%d,%d)\n", current->name, current->center.x, current->center.y, line->end.x, line->end.y);
        }
        else if (current->type == FIGURE_RECTANGLE)
        {
            Rectangle *rectangle = (Rectangle *)current;
            printf("Rectangle %s at (%d,%d) with dimensions %dx%d\n", current->name, current->center.x, current->center.y, rectangle->dimensions.width, rectangle->dimensions.height);
        }
        else if (current->type == FIGURE_TEXT)
        {
            Text *text = (Text *)current;
            printf("Text %s at (%d,%d) with text %s\n", current->name, current->center.x, current->center.y, text->text);
        }
        current = current->next;
    }
    return 0;
}

Figure *find_figure_by_name(const char *name)
{
    Figure *current = head;
    while (current != NULL)
    {
        if (strcmp(current->name, name) == 0)
        {
            return current;
        }
        current = current->next;
    }
    return NULL;
}

int delete_figure(const char *name)
{
    Figure *current = head;
    if (find_figure_by_name(name) == NULL)
    {
        return 1;
    }

    Figure *previous = NULL;
    while (current != NULL)
    {
        if (strcmp(current->name, name) == 0)
        {
            printf("Deleting figure %s\n", name);
            if (previous == NULL)
            {
                head = current->next;
            }
            else
            {
                previous->next = current->next;
            }
            free(current->name);
            return 0;
        }
        previous = current;
        current = current->next;
    }
    return 0;
}

int rename_figure(const char *old_name, const char *new_name)
{
    Figure *figure = find_figure_by_name(old_name);
    if (figure == NULL)
    {
        return 1;
    }
    if (find_figure_by_name(new_name))
    {
        return 2;
    }
    printf("Renaming figure %s to '%s'\n", old_name, new_name);
    free(figure->name);
    figure->name = strdup(new_name);
    return 0;
}

void generate_svg_circle(FILE *file, Figure *circle)
{
    Circle *c = (Circle *)circle;
    fprintf(file, "<circle cx=\"%d\" cy=\"%d\" r=\"%d\" stroke=\"%s\" stroke-width=\"%d\" fill=\"%s\" display=\"%s\" />\n", c->base.center.x, c->base.center.y, c->radius, c->base.style.stroke_color, c->base.style.stroke_width, c->base.style.fill_color, c->base.style.visible == 0 ? "" : "none");
}

void generate_svg_line(FILE *file, Figure *line)
{
    Line *l = (Line *)line;
    fprintf(file, "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"%s\" stroke-width=\"%d\" display=\"%s\" />\n", l->base.center.x, l->base.center.y, l->end.x, l->end.y, l->base.style.stroke_color, l->base.style.stroke_width, l->base.style.visible == 0 ? "" : "none");
}

void generate_svg_rectangle(FILE *file, Figure *rectangle)
{
    Rectangle *r = (Rectangle *)rectangle;
    fprintf(file, "<rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" stroke=\"%s\" stroke-width=\"%d\" fill=\"%s\" display=\"%s\" transform=\"rotate(%d, %d, %d)\" />\n", r->base.center.x, r->base.center.y, r->dimensions.width, r->dimensions.height, r->base.style.stroke_color, r->base.style.stroke_width, r->base.style.fill_color, r->base.style.visible == 0 ? "" : "none", r->base.style.rotation_angle ? r->base.style.rotation_angle : 0, r->base.style.rotation_angle ? r->base.center.x + r->dimensions.width / 2 : 0, r->base.style.rotation_angle ? r->base.center.y + r->dimensions.height / 2 : 0);
}

void generate_svg_text(FILE *file, Figure *text)
{
    Text *t = (Text *)text;
    fprintf(file, "<text x=\"%d\" y=\"%d\" fill=\"%s\" display=\"%s\" stroke-width=\"%d\" font-size=\"%d\" transform=\"rotate(%d, %d, %d)\">%s</text>\n", t->base.center.x, t->base.center.y, t->base.style.fill_color, t->base.style.visible == 0 ? "" : "none", t->base.style.stroke_width, t->font_size, t->base.style.rotation_angle, t->cx, t->cy, t->text);
}

void generate_svg_ellipse(FILE *file, Figure *ellipse)
{
    Ellipse *e = (Ellipse *)ellipse;
    fprintf(file, "<ellipse cx=\"%d\" cy=\"%d\" rx=\"%d\" ry=\"%d\" stroke=\"%s\" stroke-width=\"%d\" fill=\"%s\" display=\"%s\" />\n", e->base.center.x, e->base.center.y, e->dimensions.width, e->dimensions.height, e->base.style.stroke_color, e->base.style.stroke_width, e->base.style.fill_color, e->base.style.visible == 0 ? "" : "none");
}

char *remove_quotes(const char *filename)
{
    int length = strlen(filename);
    char *new_filename = malloc(length - 1); // Allocate memory for the new string without quotes

    if (new_filename == NULL)
    {
        fprintf(stderr, "Error: Could not allocate memory for new filename.\n");
        return NULL;
    }

    int i, j;
    for (i = 0, j = 0; i < length; i++)
    {
        if (filename[i] != '\"')
        {
            new_filename[j++] = filename[i];
        }
    }
    new_filename[j] = '\0'; // Null-terminate the new string

    return new_filename;
}

int dump_figures_to_svg(const char *filename)
{
    FILE *file = fopen(remove_quotes(filename), "w");
    if (file == NULL)
    {
        return 1;
    }
    fprintf(file, "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"800\" height=\"600\" viewBox=\"0 0 800 600\">\n");
    fprintf(file, "<rect x=\"0\" y=\"0\" width=\"800\" height=\"600\" fill=\"none\" stroke=\"black\" />\n");
    Figure *current = head;

    if (current == NULL)
    {
        return 2;
    }
    while (current != NULL)
    {
        if (current->type == FIGURE_CIRCLE)
        {
            generate_svg_circle(file, current);
        }
        else if (current->type == FIGURE_LINE)
        {
            generate_svg_line(file, current);
        }
        else if (current->type == FIGURE_RECTANGLE)
        {
            generate_svg_rectangle(file, current);
        }
        else if (current->type == FIGURE_TEXT)
        {
            generate_svg_text(file, current);
        }
        else if (current->type == FIGURE_ELLIPSE)
        {
            generate_svg_ellipse(file, current);
        }
        current = current->next;
    }
    fprintf(file, "</svg>\n");
    fclose(file);
    return 0;
}
